/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session9;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Docente
 */
public class Estudiante {
    private int id;
    private String carnet;
    private String nombres;
    private String apellidos;
    private int edad;
    private String ciudad;
    private String carrera;
    private String ruta_foto;
    private Map<String,Integer> size_map;
    
    public Estudiante() {
        sizeMap();
    }

    public Estudiante(String carnet, String nombres, String apellidos, int edad, String ciudad, String carrera, String ruta_foto) {
        this();
        this.carnet = carnet;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.edad = edad;
        this.ciudad = ciudad;
        this.carrera = carrera;
        this.ruta_foto = ruta_foto;
    }

    public Estudiante(int id, String carnet, String nombres, String apellidos, int edad, String ciudad, String carrera, String ruta_foto) {
        this();
        this.id = id;
        this.carnet = carnet;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.edad = edad;
        this.ciudad = ciudad;
        this.carrera = carrera;
        this.ruta_foto = ruta_foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getRuta_foto() {
        return ruta_foto;
    }

    public void setRuta_foto(String ruta_foto) {
        this.ruta_foto = ruta_foto;
    }
    
    private void sizeMap(){
        size_map = new HashMap<>();
        size_map.put("id", 4);
        size_map.put("carnet", 10);
        size_map.put("nombres", 20);
        size_map.put("apellidos", 20);
        size_map.put("edad", 4);
        size_map.put("ciudad", 20);
        size_map.put("carrera", 25);
        size_map.put("ruta_foto", 200);        
    }

    @Override
    public String toString() {
        return "Estudiante{" + "id=" + id + ", carnet=" + carnet + ", nombres=" + nombres + ", apellidos=" + apellidos + ", edad=" + edad + ", ciudad=" + ciudad + ", carrera=" + carrera + ", ruta_foto=" + ruta_foto + '}';
    }
    
}
