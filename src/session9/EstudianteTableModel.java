/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session9;

import java.util.Arrays;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Docente
 */
public class EstudianteTableModel extends AbstractTableModel {
    private List<Estudiante> estudiantes;
    private final String[] column_header = {"ESTUDIANTES"};
    
    public EstudianteTableModel(List<Estudiante> estudiantes) {
        this.estudiantes = estudiantes;
    }

    public EstudianteTableModel(Estudiante[] estudiantes) {
        this.estudiantes = Arrays.asList(estudiantes);
    }
        
    public void add(Estudiante e){
        estudiantes.add(e);
    }
    
    public void delete(int index){
        if(index < 0 || index >= estudiantes.size()){
            return;
        }        
        estudiantes.remove(index);        
    }
    
    @Override
    public int getRowCount() {
        return this.estudiantes.size();
    }

    @Override
    public int getColumnCount() {
        return column_header.length;        
    }

    @Override
    public Object getValueAt(int i, int i1) {
        return estudiantes == null ? null : estudiantes.get(i);
    }
    
}
