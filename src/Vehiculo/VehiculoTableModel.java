/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehiculo;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author fran
 */
public class VehiculoTableModel extends AbstractTableModel {

    private List<String> columnNames;
    private List<Vehiculo> data;

    public VehiculoTableModel() {
        super();
        columnNames = new ArrayList<>();
        data = new ArrayList<>();
    }

    public VehiculoTableModel(List<String> ColumnNames, List<Vehiculo> data) {
        this.columnNames = ColumnNames;
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public Object getValueAt(int row, int col) {

        if (data.isEmpty()) {
            return null;
        }

        if (row < 0 || row >= data.size()) {
            return null;
        }

        List<String> vehiculo = data.get(row).toList();

        if (col < 0 || col >= vehiculo.size()) {
            return null;
        }

        return vehiculo.get(col);
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        if (row < 0 || row >= data.size()) {
            return;
        }
        List<String> rowVehiculo = data.get(row).toList();
        if (col < 0 || col >= rowVehiculo.size()) {
            return;
        }

        rowVehiculo.set(col, value.toString());
        data.set(row, new Vehiculo(rowVehiculo));
        fireTableCellUpdated(row, col);

    }

    @Override
    public String getColumnName(int column) {
        return columnNames.get(column);
    }

    public int addRow() {
        return addRow(new Vehiculo());
    }

    public int addRow(Vehiculo row) {
        data.add(row);
        fireTableRowsInserted(data.size() - 1, data.size() - 1);
        return data.size() - 1;
    }

    public void deleteRow(int row) {
        if (row < 0) {
            return;
        }

        data.remove(row);
        fireTableRowsDeleted(row, row);
    }

    public void loadFromJson() throws FileNotFoundException {
        Gson gson = new Gson();
        data.addAll(Arrays.asList(gson.fromJson(new FileReader("resources/Vehiculo.json"), Vehiculo[].class)));
        String[] names = {"codigo", "marca", "modelo", "motor", "desplazamiento", "velocidad", "cilindros"};
        columnNames = Arrays.asList(names);
    }

    public void updateData() throws FileNotFoundException, IOException {
        try (Writer jsonWriter = new FileWriter("resources/Vehiculo.json")) {
            Gson gson = new Gson();
            JsonArray result = (JsonArray) gson.toJsonTree(data,
            new TypeToken<List<Vehiculo>>() {
            }.getType());
            gson.toJson(result, jsonWriter);
        }
    }

}
