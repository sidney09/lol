/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vehiculo;

import componentes.jtable.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fran
 */
public class Vehiculo {

    private int id;
    private String marca;
    private String modelo;
    private String motor;
    private String desplazamiento;
    private String velocidad;
    private String cilindros;

    public Vehiculo() {
    }

    public Vehiculo(List<String> docenteAsList) {
        this(Integer.parseInt(docenteAsList.get(0)), docenteAsList.get(1), docenteAsList.get(2),
                docenteAsList.get(3), docenteAsList.get(4), docenteAsList.get(5), docenteAsList.get(6));
    }

    public Vehiculo(int codigo, String nombres, String apellidos) {
        this.id = codigo;
        this.marca = nombres;
        this.modelo = apellidos;
    }

    public Vehiculo(int codigo, String nombres, String apellidos, String cedula, String direccion, String telefono, String correo) {
        this.id = codigo;
        this.marca = nombres;
        this.modelo = apellidos;
        this.motor = cedula;
        this.desplazamiento = direccion;
        this.velocidad = telefono;
        this.cilindros = correo;
    }

    public int getCodigo() {
        return id;
    }

    public void setCodigo(int id) {
        this.id = id;
    }

    public String getNombres() {
        return marca;
    }

    public void setNombres(String nombres) {
        this.marca = nombres;
    }

    public String getApellidos() {
        return modelo;
    }

    public void setApellidos(String apellidos) {
        this.modelo = apellidos;
    }

    public String getCedula() {
        return motor;
    }

    public void setCedula(String cedula) {
        this.motor = cedula;
    }

    public String getDireccion() {
        return desplazamiento;
    }

    public void setDireccion(String direccion) {
        this.desplazamiento = direccion;
    }

    public String getTelefono() {
        return velocidad;
    }

    public void setTelefono(String telefono) {
        this.velocidad = telefono;
    }

    public String getCorreo() {
        return cilindros;
    }

    public void setCorreo(String correo) {
        this.cilindros = correo;
    }

    public List<String> toList() {
        List<String> vehiculo = new ArrayList<>();

        vehiculo.add(String.valueOf(id));
        vehiculo.add(marca);
        vehiculo.add(modelo);
        vehiculo.add(motor);
        vehiculo.add(desplazamiento);
        vehiculo.add(velocidad);
        vehiculo.add(cilindros);

        return vehiculo;

    }

}
