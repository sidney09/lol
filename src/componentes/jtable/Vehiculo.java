/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componentes.jtable;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Yasser
 */
public class Vehiculo {

    private int codigo;
    private String nombres;
    private String apellidos;
    private String cedula;
    private String direccion;
    private String telefono;
    private String correo;

    public Vehiculo() {
    }

    public Vehiculo(List<String> docenteAsList) {
        this(Integer.parseInt(docenteAsList.get(0)), docenteAsList.get(1), docenteAsList.get(2),
                docenteAsList.get(3), docenteAsList.get(4), docenteAsList.get(5), docenteAsList.get(6));
    }

    public Vehiculo(int codigo, String nombres, String apellidos) {
        this.codigo = codigo;
        this.nombres = nombres;
        this.apellidos = apellidos;
    }

    public Vehiculo(int codigo, String nombres, String apellidos, String cedula, String direccion, String telefono, String correo) {
        this.codigo = codigo;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.cedula = cedula;
        this.direccion = direccion;
        this.telefono = telefono;
        this.correo = correo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public List<String> toList() {
        List<String> docente = new ArrayList<>();

        docente.add(String.valueOf(codigo));
        docente.add(nombres);
        docente.add(apellidos);
        docente.add(cedula);
        docente.add(direccion);
        docente.add(telefono);
        docente.add(correo);

        return docente;

    }

}
